#include "player.h"
#include "location.h"
#include "parser.h"
#include <cstddef>
#include "initMap.h"
#include <stdlib.h>
#include <iostream>
#include "command.h"
#include "narrate.h"

int main(){
	
	Location *start;
	start = initMap();
	std::cout << "Welcome to... uhm... the game thing. Feel free to move around, I guess. Just say n,e,s,w cause I'm lazy and anything else will probably break the game.\n";
	Player player;
	player.cur_loc = start;
	std::string input;
	narrate(player);
	std::cout << "\n";
	Command command;
	command.player = &player;

	while(1==1){	
		std::cout << ">";
		get_line(input);		
		command.parseStr(input);
		command.executeCmd();
		narrate(player);
		std::cout << "\n";
		command.reset();
	}
	return 0;
}	
		
