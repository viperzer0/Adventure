#include "location.h"
#include "initMap.h"
#include "item.h"
Location *initMap(void){
	//This code will link all of the map locations together when the program starts.
	Location *startPtr = new Location();
	startPtr->addName("Main Room");
	startPtr->addDesc("An unaverage room, for it has blood stains across the walls, a horrifically ugly chandelier, and a penguin poster. There's a door to the north.");
	
	Location *kitchenPtr = new Location();
	kitchenPtr->addName("Kitchen");
	kitchenPtr->addDesc("A moderately average kitchen, except for the human corpses hanging from the ceiling. It's pretty neat. There's a wooden door facing south and a very suspicious looking gateway that seems to lead to oblivion facing east. And a magic door to the west. Ooh, it's singing!");

	Location *lairPtr = new Location();
	lairPtr->addName("Dragon's Lair");
	lairPtr->addDesc("A devilishly sinister dragon's lair. Inside is a dragon. He looks hungry.");

	Location *fairyPtr = new Location();
	fairyPtr->addName("Fairy Forest");
	fairyPtr->addDesc("A forest full of fanciful fairies. Feel free to fraternize with the friendly faries. There's a doorway to the east.");

	startPtr->connectTo(kitchenPtr,NULL,NULL,NULL);
	kitchenPtr->connectTo(NULL,lairPtr,startPtr,fairyPtr);
	lairPtr->connectTo(NULL,NULL,NULL,kitchenPtr);		
	fairyPtr->connectTo(NULL,kitchenPtr,NULL,NULL);	

	Item *cookie = new Item();
	cookie->addName("cookie");
	cookie->addDesc(INIT,"There's a cookie sitting on the table.");
	cookie->addDesc(HELD,"A delicious looking chocolate chip cookie.");
	cookie->addDesc(DROP,"There's a cookie on the floor. Five second rule?");
	cookie->addCat(FOOD);
	kitchenPtr->addItem(cookie);
	
	Item *sword = new Item();
	sword->addName("sword");
	sword->addDesc(INIT,"A huge sword hangs from the wall.");
	sword->addDesc(HELD,"A large sword");
	sword->addDesc(DROP,"There's a sword lying on the ground.");
	sword->addCat(WEAPON);
	startPtr->addItem(sword);

	return startPtr; //Starting Location
}
