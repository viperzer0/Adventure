#include "item.h"

void Item::printDesc(){
	switch(curDesc){
		case INIT:
			std::cout << desc;
			break;
		case HELD:
			std::cout << heldDesc;
			break;
		case DROP:
			std::cout << dropDesc;
			break;
		default:
			std::cout << desc;
			break;
	}
}

void Item::addDesc(itemPos_t cat,char const *input){
	switch(cat){
		case INIT:
			desc = input;
			break;
		case HELD:
			heldDesc = input;
			break;
		case DROP:
			dropDesc = input;
			break;
		default:
			desc = input;
			break;
	}
}

Item::Item(){
	heldDesc = "";
	dropDesc = "";
	curDesc = INIT;
	category = OTHER;
}

void Item::addCat(item_t cat){
	category = cat;
}
