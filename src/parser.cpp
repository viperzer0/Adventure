#include "location.h"
#include "parser.h"
#include <ctype.h>
#include "nav.h"
#include "command.h"
#include <string>
#include <iostream>
void get_line(std::string &input){
  		std::getline(std::cin,input);
		for(int i=0; i<input.length(); i++)
			input[i]=tolower(input[i]);
}
void Command::reset(){
	verb = "";
	target = "";
	tool = "";
}
void Command::parseStr(std::string input){
	int numWords = countWords(input);
	if(isTootsie(input)) tootsieCrash();
	if(direction(input)!=ERR){
		player->Move(direction(input));
	}
	if(numWords==1){ //If there's one word, format is VERB
		if(direction(parseWord(input, 0))!=ERR){
			verb="go";
			target=parseWord(input, 0);
		}
		else{
			verb=parseWord(input, 0);
		}
	}
	if(numWords==2){ //If there's exactly two words format is VERB TARGET 
		verb=parseWord(input, 0);
		target=parseWord(input, 1);
	}
	if(numWords==3){ 
		//IDK
	}
	if(numWords==4){ //If there's four words format is either VERB TARGET with TOOL or VERB TOOL on/in/at TARGET
		verb = parseWord(input, 0);
		if(parseWord(input,2)=="with"){
			target=parseWord(input,1);
			tool = parseWord(input,3);
		}
		else{
			tool = parseWord(input,1);
			target=parseWord(input,3);
		}
	}
	if(numWords>4){ //In cases of google "really large input that does things."
		verb = parseWord(input, 0);
		//Figure out what to do from there.
	}

}

void Command::executeCmd(){
	if(verb=="google")
		google();
	if(verb=="exercise")
		exercise();
	if(verb=="exorcise")
		exorcise();
	if(verb=="dance")
		dance();
	if(verb=="take"){
		Item *item = player->cur_loc->findItem(target);
		if(item){
			player->takeItem(item);
			player->cur_loc->remItem(item);	
		}
		else std::cout << "Take what?\n";
	}
	if(verb=="freak")
		if(target=="out")
			freakOut();

	if(verb=="inventory"||verb=="inv")
		player->displayInv();

}

int countWords(std::string input){
	int count=1,i=0;
	char curChar=input[0];
	while(curChar!='\0'&&curChar!='\n'){
		if(curChar==' ') count++;
		i++;
		curChar = input[i];
	}		
	return count;
}

std::string parseWord(std::string input, int pos){
	int startPos=0,endPos=0,index=0;
	char curChar=input[0];
	while(index<pos){
		while(curChar!=' '&&curChar!='\n'&&curChar!='\0'){
			endPos++;
			curChar=input[endPos];
		}
		index++;
		endPos++;
		startPos=endPos;
		curChar=input[startPos];	
	}
	while(curChar!=' '&&curChar!='\n'&&curChar!='\0'){ //Last execution of parser will get word at the proper position
			endPos++;
			curChar=input[endPos];
	}
	int len=endPos-startPos;
	std::string word;
	return input.substr(startPos,len); //Return OBJECT, not pointer
}


