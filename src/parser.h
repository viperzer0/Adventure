#ifndef PARSER_H
#define PARSER_H
#include "player.h"
void get_line(std::string &input);
compass_t direction(char input);

class Command{
	std::string verb;
	std::string target; 
	std::string tool; 
	public:
		Player *player; //Reference?
		void parseStr(std::string);
		void executeCmd();
		void reset();
};
static const char * const articles[] = {"a","an","the"};

int countWords(std::string input);
std::string parseWord(std::string input, int pos);
int findItem(Location *room, std::string name);

#endif
