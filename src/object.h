#ifndef OBJECT_H
#define OBJECT_H

#include <string>
#include <iostream>
#include <bitset>
class Object{
	protected:	
		std::string desc;	
		std::bitset<8> status; //Can represent various on-off statuses for various porposies.
		std::string name; //Needs to be accessible
	public:
		void print(){
			std::cout << name << "\n";
			std::cout << desc;
		}
		void printName(){
			std::cout << name;
		}
		virtual void printDesc(){
			std::cout << desc;
		}	
		void addName(char const *newName){
			name = newName;
		}
		virtual void addDesc(char const *newDesc){
			desc = newDesc;
		}
		std::string getName(){return name;}

};
#endif
