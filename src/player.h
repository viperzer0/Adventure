#ifndef PLAYER_H
#define PLAYER_H
#include "location.h"
class Player: public Object{
	std::vector<Item *> inventory;
	public:
		Location *cur_loc;
		int Move(compass_t dir);
		void takeItem(Item *);
		void displayInv();
};

#endif							
