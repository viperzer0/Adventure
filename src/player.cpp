#include "player.h"
#include "location.h" 

int Player::Move(compass_t dir){
	Location *moveTo = cur_loc->mover(dir);
	if(!moveTo)
		return -1; //Got NULL pointer, i.e, moving that direction is impossible.
	else{
		cur_loc=moveTo;
		return 0;
	}
}

void Player::takeItem(Item *item){
	item->setPos(HELD);
	inventory.push_back(item);
}	

void Player::displayInv(){
	std::cout << "You have:\n";
	for(int i=0;i<inventory.size();i++){
		std::cout << "\t";
		inventory[i]->printDesc();
		std::cout << "\n";
	}
}	
		
