#include "nav.h"
compass_t direction(std::string input){
	if((input=="north"||input=="n")) return NORTH;
	else if((input=="east"||input=="e")) return EAST;
	else if((input=="south"||input=="s")) return SOUTH;
	else if((input=="west"||input=="w")) return WEST;
	else if((input=="up"||input=="u")) return UP;
	else if((input=="down"||input=="d")) return DOWN;
	else return ERR;
}

