#ifndef ITEM_H
#define ITEM_H
#include "object.h"

enum item_t {FOOD,WEAPON,TOOL,OTHER};
enum itemPos_t {INIT,HELD,DROP};

class Item: public Object{
	std::string heldDesc; 
	std::string dropDesc; //Two additional descriptions for inventory and dropped
	itemPos_t curDesc; //0=Initial desc, 1=In inventory, 2=Dropped	
	public:	
		item_t category;
		Item();
		void printDesc();
		void addDesc(itemPos_t,char const *);
		void addCat(item_t cat);
		void setPos(itemPos_t stat){
			curDesc=stat;
		}
};						
	
#endif
