#ifndef LOCATION_H
#define LOCATION_H
#include <cstddef>
#include <cstring>
#include "object.h"
#include "nav.h"
#include <vector>
#include "item.h"

class Location: public Object{
	Location *north;
	Location *east;
	Location *south;
	Location *west;
	Location *up;
	Location *down;
	//In THIS object, we have six bits for whether locations are CURRENTLY allowed or not, rather than rearranging pointers to be NULL or the proper location. (Though maybe that's not a bad idea?)
	public:	
		std::vector<Item *> items; //Pointers to items in the room
		Location();
		void connectTo(Location *n, Location *e, Location *s, Location *w, Location *u=NULL, Location *d=NULL);
		Location *mover(compass_t dir);
		void printCont();
		void addItem(Item *item);
		Item *findItem(std::string nme);
		void remItem(Item *item);  		
};

Location *init_Map(void);
#endif
