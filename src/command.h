#ifndef COMMAND_H
#define COMMAND_H
#include <string>
#include "item.h"
#include "player.h"
void tootsieCrash();
bool isTootsie(std::string input);
void google();
void exercise();
void exorcise();
void dance();
void freakOut();
void take(Player &player, Item *item);


#endif

