#include "location.h"
#include <stdlib.h>
#include <stdio.h>

Location::Location(){
	north = NULL;
	east = NULL;
	south = NULL;
	west = NULL;
	up = NULL;
	down = NULL;	
}

void Location::connectTo(Location *n, Location *e, Location *s, Location *w, Location *u, Location *d){
	//TODO: Make linking rooms together easier?
	north = n;
	east = e;
	south = s;
	west = w;
	up = u;
	down = d;
	status= 0x3F; //Start off with 00111111 status
}

Location *Location::mover(compass_t dir){
	switch(dir){
		case NORTH:
			if(status[0]==1)
				return north;
			else
				return NULL;
		case EAST:
			if(status[1]==1)
				return east;
			else
				return NULL;
		case SOUTH:
			if(status[2]==1)
				return south;
			else
				return NULL;
		case WEST:
			if(status[3]==1)
				return west;
			else
				return NULL;
		case UP:
			if(status[4]==1)
				return up;
			else
				return NULL;
		case DOWN:
			if(status[5]==1)
				return down;
			else
				return NULL;
	
		default: //Should never happen but lets double check
			return NULL;
	}

}

void Location::printCont(){
	for(int i=0;i<items.size();i++){
		std::cout << " "; //Print space cuz reasons
		items[i]->printDesc();
	}
}

void Location::addItem(Item *item){
	items.push_back(item);
}

Item *Location::findItem(std::string name){
	for(int i=0;i<items.size();i++){
		if(items[i]->getName()==name)
			return items[i];
	}
	return NULL;
}

void Location::remItem(Item *item){
	for(int i=0;i<items.size();i++){
		if(items[i]==item)
			items.erase(items.begin()+i);
	}
}
