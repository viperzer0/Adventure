#ifndef NAV_H
#define NAV_H
#include <string>
enum compass_t {NORTH, EAST, SOUTH, WEST, UP, DOWN, ERR=-1};

compass_t direction(std::string input);

#endif
